# springboot

#### 项目介绍
使用spring boot集成了shiro + redis的前后端分离系统框架demo

#### 软件架构
● 核心框架： spring boot + spring

● mvc框架： spring mvc

● 持久层框架： mybatis

● 数据库连接池： alibaba druid

● 安全框架： apache shiro

● 缓存框架： redis

● 日志框架： logback


#### 使用说明

导入项目后，使用maven下载相关jar包，直接运行SpringbootApplication.java即可


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)